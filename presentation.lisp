(declaim (ftype (function (number number) number) add))
(defun add (x y)
  (declare (number x y))
  (the number (+ x y)))
(defun add2 (x y)
  (+ x y))
(defclass dog ()
  ((name :reader name :initarg :name :type string)
   (age :accessor age :type fixnum :initarg :age)))
(defgeneric speak (thing))
(defmethod speak ((rover dog))
  (dotimes (times (age rover))
    (format t "~A SPEAK!~%" (name rover))
    (format t "WOOF!~%")))
(let ((our-dog
        (make-instance 'dog :name "Stupid dog!" :age 1)))
  (speak our-dog)
  ; Stupid dog! SPEAK!
  ; WOOF!
  (incf (age our-dog))
  (speak our-dog)
  ; Stupid dog! Speak!
  ; WOOF!
  ; Stupid dog! Speak!
  ; WOOF!
  )
