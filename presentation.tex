% Local Variables:
% TeX-command-extra-options: "-shell-escape"
% End:
\documentclass{beamer}
\title{Modern Game Dev with Lisp}
\subtitle{A lisp propaganda piece}
\author{Lily Carpenter}
\institute{https://www.azrazalea.net}
\mode<presentation> {\usetheme{Dresden}}
\date{2016-02-04}

\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{upquote}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Summary}
  \tableofcontents
\end{frame}

\begin{frame}
  \frametitle{Get my presentation here!}
  \href{https://gitlab.com/azrazalea/lisp-game-dev-presentation}{https://gitlab.com/azrazalea/lisp-game-dev-presentation}
\end{frame}
\section{What is lisp?}

\begin{frame}
  \frametitle{John McCarthy(1927-09-04 to 2011-10-24)}
  \begin{center}
    \includegraphics[scale=0.33]{john-mccarthy-programming-wrong}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{History}
  \begin{itemize}
  \item Lisp (not Common Lisp) was originally implemented in the early 1960s.
  \item Really took off in the 1970s and 1980s.
  \item Lots of differing implementations with their own ideas and features during this time period.
  \item Common Lisp became an ANSI standard in 1994.
  \item Historically used primarily for Artificial Intelligence.
  \item Use drastically lowered after the decline of AI funding when the world realized ``true AI'' was further off than they thought.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Made with Secret Alien Technology}
  \begin{center}
    \includegraphics[scale=0.75]{lisplogo_256}
  \end{center}
\end{frame}

\begin{frame}[fragile]
  \frametitle{An example}
\begin{minted}[gobble=4,fontsize=\footnotesize]{cl}
    ;; From http://rosettacode.org/wiki/Fibonacci_sequence
    (defun fibonacci-tail-recursive (n &optional (a 0) (b 1))
      (if (= n 0)
        a
        (fibonacci-tail-recursive (- n 1) b (+ a b))))
\end{minted}
\end{frame}

\begin{frame}
  \frametitle{Lisp Implementations}
  \begin{itemize}
  \item Common Lisp is an ansi standard.
  \item There are many implementations of this standard, both commercial and open source.
  \item The current most popular appear to be \href{http://sbcl.org/}{SBCL} and \href{http://ccl.clozure.com/}{CCL}.
  \item There are many differences between implementations, but they are generally easy to deal with using libraries built for the purpose.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Lisp Libraries}
  \begin{itemize}
  \item Quicklisp is the library manager for Lisp. \href{https://beta.quicklisp.org}{https://beta.quicklisp.org}.
  \item While still marked as ``beta'' it is stable and usable. It just lacks some features the author wants to add before considering it released.
  \item Coolest feature is the ability to install + load libraries completely from the Lisp REPL without restarting the process.
  \end{itemize}
\end{frame}

\section{Useful Lisp Features}

\begin{frame}[fragile]
  \frametitle{Compiled and Interpreted (simultaneously)}
\begin{minted}{lisp}
 (disassemble (defun add () (+ 2 4)))
; disassembly for ADD
; Size: 22 bytes. Origin: #x1003BA0A04
; 04:       498B4C2460       MOV RCX, [R12+96]                ; thread.binding-stack-pointer
                                                              ; no-arg-parsing entry point
; 09:       48894DF8         MOV [RBP-8], RCX
; 0D:       BA0C000000       MOV EDX, 12
; 12:       488BE5           MOV RSP, RBP
; 15:       F8               CLC
; 16:       5D               POP RBP
; 17:       C3               RET
; 18:       CC10             BREAK 16                         ; Invalid argument count trap
\end{minted}
\end{frame}

\begin{frame}
  \frametitle{"Gradual Typing"}
  \begin{itemize}
  \item Can declare types of arguments and function returns values. Commonly only argument types are declared.
  \item Type system integrated with Object System, used for generics/method dispatch.
  \item In SBCL types are verified at compile time in certain cases, some other implementations only verify at run time.
  \item SBCL will use types to optimize the compiled code, resulting in large performance improvements in some cases.
  \item Unfortunately compile-time checking is not that advanced, mostly only catches constants/literals.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Types Example}
\begin{minted}{lisp}
(declaim (ftype (function
 (number number)
 number) add))
(defun add (x y)
 (declare (number x y))
 (the number (+ x y)))
; NIL
(add 1 2)
; 3
(add "string" 2)
; The value "string" is not of type NUMBER.
\end{minted}
\end{frame}

\begin{frame}
  \frametitle{Object Oriented programming}
  \begin{itemize}
  \item Classes have slots (``instance variables'')
  \item Methods are defined in terms of generic functions.
  \item Uses multiple dispatch instead of single.
  \item Has multiple inheritance.
  \item Has cool :before, :after, and :around methods
  \item Super dynamic, can change class definitions at runtime even when instances already exist.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Simple CLOS Example}
\begin{minted}[fontsize=\footnotesize]{lisp}
(defclass dog ()
  ((name :reader name :initarg :name :type string)
   (age :accessor age :type fixnum :initarg :age)))
(defgeneric speak (thing))
(defmethod speak ((rover dog))
  (dotimes (times (age rover))
    (format t "~A SPEAK!~%" (name rover))
    (format t "WOOF!~%")))
(let ((our-dog
        (make-instance 'dog :name "Stupid dog!" :age 1)))
  (speak our-dog)
  ; Stupid dog! SPEAK!
  ; WOOF!
  (incf (age our-dog))
  (speak our-dog)
  ; Stupid dog! Speak!
  ; WOOF!
  ; Stupid dog! Speak!
  ; WOOF!
  )
\end{minted}
\end{frame}

\begin{frame}
  \frametitle{Package System}
  \begin{itemize}
  \item Basically a namespace/module system. Using it may seem similar to clojure or javascript's ES6 modules.
  \item The way it actually works under the hood is super cool and different from most others.
  \item See \href{http://www.gigamonkeys.com/book/programming-in-the-large-packages-and-symbols.html}{Practical Common Lisp} for more info.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{C Foreign Function Interface}
  \begin{itemize}
  \item Lisp has an excellent CFFI library found at \href{https://common-lisp.net/project/cffi/}{https://common-lisp.net/project/cffi/}. It is in quicklisp.
  \item The CFFI is used heavily for game development, as OpenGL and other libraries are C.
  \item Cool library \href{https://github.com/rpav/cl-autowrap}{https://github.com/rpav/cl-autowrap} makes making a new CFFI wrapper super easy.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The REPL}
  \begin{itemize}
  \item The sheer power of the Common Lisp REPL is greater than any other REPL I know of.
  \item REPL driven development is THE primary paradigm for common lisp (no this is not really at odds with TDD).
  \item You can do practically anything in the REPL that you could do in a file, all without having to restart your process.
  \item For a user friendly REPL experience you will want to use Emacs + Slime, Vim + slimv, or one of the few lisp specific IDEs.
  \item The default command line REPLs for most implementations have a rather bad user experience compared to the above.
  \end{itemize}
\end{frame}

\section{Lisp game tools}
\begin{frame}
  \frametitle{Lispgames Group}
  \begin{itemize}
  \item \href{https://github.com/lispgames}{https://github.com/lispgames}
  \item Benevolent leader rpav/oGMo \href{https://github.com/rpav}{https://github.com/rpav}
  \item Freenode IRC channel \#lispgames
  \item In progress wiki/site \href{http://lispgames.org}{http://lispgames.org}
  \item Focused on Common Lisp but all lispers welcome including Scheme, Clojure, and others.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Libraries}
  \begin{itemize}
  \item For the most part if you don't want to use a pre-built engine/framework you probably want to use lispgames glkit which uses cl-sdl2, cl-sdl2kit, and cl-opengl. Find it at \href{https://github.com/lispgames/glkit}{https://github.com/lispgames/glkit}. They are all in quicklisp.
  \item A cool 2D graphics library called sketch can be found at \href{https://github.com/vydd/sketch}{https://github.com/vydd/sketch}. It is not yet in quicklisp.
  \item There are a variety of other libraries in various locations, if you are looking for something in particular ask about it on \#lispgames.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Game Engines}
  \begin{itemize}
  \item CEPL by baggers, \href{https://github.com/cbaggers/cepl}{https://github.com/cbaggers/cepl}. Active work in progress.
  \item CLINCH by warweasle, \href{https://github.com/BradWBeer/CLinch}{https://github.com/BradWBeer/CLinch}. Alpha, should soon be ready as a general purpose engine.
  \item Xelf by dto (see dto-games on youtube), GPLv3, \href{http://xelf.me/}{http://xelf.me/}. Has been used by dto to create several games.
  \end{itemize}
\end{frame}

\section{Learning resources}
\begin{frame}
  \frametitle{Books}
  \begin{itemize}
  \item The de facto primary learning book is ``Practical Common Lisp'' and is free online at \href{http://www.gigamonkeys.com/book/}{http://www.gigamonkeys.com/book/}
  \item There is a brand new (not free) book being hailed as an excellent follow up called ``Common Lisp Recipes'' at \href{http://www.apress.com/9781484211779?gtmf=s}{http://www.apress.com/9781484211779?gtmf=s}
  \item A list of other Lisp books can be found at \href{http://cliki.net/Lisp+Books}{http://cliki.net/Lisp+Books}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Videos}
  \begin{itemize}
  \item \href{https://www.youtube.com/channel/UCKfZ9JfFgg7cxa2hYfC5O0A}{dto-games} channel on youtube, live coding and commentary.
  \item cbaggers user \href{https://www.youtube.com/user/CBaggers}{https://www.youtube.com/user/CBaggers} on youtube. Demonstrations of the CEPL game engine, general tutorials.
  \item Axed Code (axion) short progress videos \href{https://www.youtube.com/playlist?list=PLcdTh590GDjFgdM8JVSBqFl4vJBrh8P_l}{playlist} on youtube.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Websites}
  \begin{itemize}
  \item Common Lisp \href{http://www.lispworks.com/documentation/HyperSpec/Front/index.htm}{Hyperspec}
  \item \href{http://articulate-lisp.com/}{http://articulate-lisp.com/}
  \item Common Lisp wiki \href{http://www.cliki.net/}{http://www.cliki.net}
  \item Common Lisp official site \href{https://common-lisp.net/}{https://common-lisp.net/}
  \item Common Lisp gitlab instance \href{https://gitlab.common-lisp.net}{https://gitlab.common-lisp.net}
  \end{itemize}
\end{frame}

\section{Demonstration}
\begin{frame}
  \frametitle{Demonstration}
  \begin{itemize}
  \item I will be running and modifying \href{https://github.com/k-stz/picking-sticks}{https://github.com/k-stz/picking-sticks}.
  \item You will need a lisp implementation, \href{https://www.quicklisp.org/beta/}{quicklisp}, libsdl2, libfreetype, working OpenGL drivers, and to put \href{https://github.com/rpav/texatl}{https://github.com/rpav/texatl} and \href{https://github.com/rpav/laconic}{https://github.com/rpav/laconic} in \texttildelow/quicklisp/local-projects.
  \item Unless you've already got most of this setup I would recommend not trying to set this up during my presentation.
  \end{itemize}
\end{frame}
\end{document}
